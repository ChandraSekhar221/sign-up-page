const fNameInputEl = document.querySelector('#fName')
const lNameInputEl = document.querySelector('#lName')
const emailInputEl = document.querySelector('#email')
const passwordInputEl = document.querySelector('#password')
const rPasswordInputEl = document.querySelector('#rPassword')
const checkboxEl = document.querySelector('#checkbox')

const errMessagesNodeList = document.getElementsByTagName('p')


const validateDetails = () => {
    errMessagesNodeList.namedItem('successMsg').textContent = ""
    let password = passwordInputEl.value.trim()
    if (fNameInputEl.value.trim() == "") {
        errMessagesNodeList.namedItem('fNameErrMsg').textContent = '*Enter Your First Name'
    }
    else {
        if (!fNameInputEl.value.trim().match(/^[a-zA-Z]+$/)) {
            errMessagesNodeList.namedItem('fNameErrMsg').textContent = "*Enter Valid First Name, should not contains any special characters and numbers"
        }
        else {
            errMessagesNodeList.namedItem('fNameErrMsg').textContent = ""
        }
    }
    if (lNameInputEl.value.trim() == "") {
        errMessagesNodeList.namedItem('lNameErrMsg').textContent = "*Enter Your Last Name"
    }
    else {
        if (!lNameInputEl.value.trim().match(/^[a-zA-Z]+$/)) {
            errMessagesNodeList.namedItem('lNameErrMsg').textContent = "*Enter Valid Last Name, should not contains any special characters and numbers"
        }
        else {
            errMessagesNodeList.namedItem('lNameErrMsg').textContent = ""
        }
    }
    if (emailInputEl.value.trim() == "") {
        errMessagesNodeList.namedItem('emailErrMsg').textContent = "*Enter Your Email Address"
    }
    else {
        if (!emailInputEl.value.trim().toLowerCase().match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
            errMessagesNodeList.namedItem('emailErrMsg').textContent = "*Enter Valid Email Address"
        } else {
            errMessagesNodeList.namedItem('emailErrMsg').textContent = ""
        }
    }
    if (passwordInputEl.value.trim() == "") {
        errMessagesNodeList.namedItem('passwordErrMsg').textContent = "*Enter Your Password"
        errMessagesNodeList.namedItem('passwordCriteria').textContent = ""

    }
    else {
        if (password.match(/[0-9]/g) && password.match(/[a-z]/g) && password.match(/[0-9]/g) && password.length >= 8 ) {
            errMessagesNodeList.namedItem('passwordErrMsg').textContent = ""
            errMessagesNodeList.namedItem('passwordCriteria').textContent = ""
        }
        else {
            errMessagesNodeList.namedItem('passwordErrMsg').textContent = "*Enter Valid Password"
            errMessagesNodeList.namedItem('passwordCriteria').textContent = "*Password must contain at least one number and one uppercase and lowercase letter, and greter than 8 characters"
        }
    }
    if (rPasswordInputEl.value.trim() == "") {
        errMessagesNodeList.namedItem('rPasswordErrMsg').textContent = "*Re-enter Your Password"
    }
    else {
        errMessagesNodeList.namedItem('rPasswordErrMsg').textContent = ""
    }
    if ((rPasswordInputEl.value.trim() != "") && (passwordInputEl.value != rPasswordInputEl.value)) {
        errMessagesNodeList.namedItem('rPasswordErrMsg').textContent = "*Password mismatch"
    }
    if (!checkboxEl.checked) {
        errMessagesNodeList.namedItem('checkboxErrMsg').textContent = "*Accept terms and conditions.."
    }
    else {
        errMessagesNodeList.namedItem('checkboxErrMsg').textContent = ""
    }
    if(
        fNameInputEl.value.trim() != "" && 
        fNameInputEl.value.trim().match(/^[a-zA-Z]+$/) &&
        lNameInputEl.value.trim() != "" &&
        lNameInputEl.value.trim().match(/^[a-zA-Z]+$/) &&
        emailInputEl.value.trim() != "" &&
        emailInputEl.value.trim().toLowerCase().match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
        && passwordInputEl.value.trim() != "" && 
        (password.match(/[0-9]/g) && password.match(/[a-z]/g) && password.match(/[0-9]/g) && password.length >= 8 ) &&
        ((rPasswordInputEl.value.trim() != "") && (passwordInputEl.value == rPasswordInputEl.value)) && 
        checkboxEl.checked
    ){
        errMessagesNodeList.namedItem('successMsg').textContent = "Account Created Successfully"
        errMessagesNodeList.namedItem('successMsg').style.color = "blue"
        fNameInputEl.value = ""
        lNameInputEl.value = ""
        emailInputEl.value = ""
        passwordInputEl.value = ""
        rPasswordInputEl.value = ""
        checkboxEl.checked = false

    }
}